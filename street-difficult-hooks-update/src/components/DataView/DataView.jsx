
import React from "react";
import "./DataView.css";

const DataView = (props) => {
  const renterRatio = Math.round((props.renterCost / props.renterIncome) * 100);
  const ownerRatio = Math.round((props.ownerCost / props.ownerIncome) * 100);
  return (
    <div className="view">
      {/* left side of card */}
      <div className="view-column">
        <div className="borough">
          <h1 className="borough-text">{props.borough}</h1>
        </div>
      </div>
      {/* right side of card */}
      <div className="view-column">
        <table className="data-view">
          <tr className="data-view-row">
            <th className="text">
              <h3 className="text-header">Renters</h3>
            </th>
            <th className="text">
              <h3 className="text-header">Homeowners</h3>
            </th>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{props.renterCost}</h2>
              <h3 className="text-label">Annual Housing Cost</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{props.ownerCost}</h2>
              <h3 className="text-label">Annual Housing Cost</h3>
            </td>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{props.renterIncome}</h2>
              <h3 className="text-label">Annual Income</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{props.ownerIncome}</h2>
              <h3 className="text-label">Annual Income</h3>
            </td>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{renterRatio}</h2>
              <h3 className="text-label">Cost-To-Income Ratio</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{ownerRatio}</h2>
              <h3 className="text-label">Cost-To-Income Ratio</h3>
            </td>
          </tr>
        </table>
        <p>
          {props.tagline}
        </p>
      </div>
    </div>
  );
};

export default DataView;
